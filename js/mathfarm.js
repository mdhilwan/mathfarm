// Debug Mode
var DM = false;
var aid;

$(document).ready(function(){
    
    $('.dropdown-menu').find('li').click(function (e) {
		e.stopPropagation();
	});

	$('.btn-bookmark').click(function (e) {
		$('#bookmarksList').toggleClass('off');
	})

	math.config({
		number: 'fraction' 
	});

});

(function(angular) {
	'use strict';

	var mfa = angular.module('mathfarmApp', ['ngSanitize', 'facebook', 'firebase']);

	mfa.config([
	    'FacebookProvider',
	    function(FacebookProvider) {
	        FacebookProvider.init((document.domain == 'mathfarm.col-md-12.com' ? '1687172851562570' : '1677139985899190'));
	    }
	])

	mfa.controller('mathfarmController', ['$scope','$http','$firebaseObject','Facebook', 
		function($scope, $http, $firebaseObject, Facebook) {
		    
		    var mathfarm = this,
		    	userIsConnected = false,
		    	ref = new Firebase("https://flickering-inferno-2465.firebaseio.com");

			mathfarm.numbers = [];
			mathfarm.answerProvided = '';
			mathfarm.correctAnswer = 0;

			$scope.nextQuestionMsg = "Skip";
			$scope.mathJax = '';

			$scope.showShareFB = 'hidden';

			var obj = $firebaseObject(ref);

			obj.$bindTo($scope, "data").then(function() {
				console.log($scope.data); 
				$scope.data.foo = "baz";  
				ref.set({ foo: "baz" });  
			});

			// Number of values
		    $scope.numberOfValues = {
		    	length:5, digits:1, decimals:false,
		    	minVal:2, maxVal:10,
		    	minDigit:1, maxDigit:3
		    };

		    $(".value-slider").slider({
		    	min: $scope.numberOfValues.minVal,
      			max: $scope.numberOfValues.maxVal,
      			value: $scope.numberOfValues.length,
      			slide: function (evt, ui) {
      				$scope.$apply(function () {
      					$scope.numberOfValues.length = ui.value;	
      				})
      			},
      			stop: function (evt, ui) {
      				mathfarm.generateNumbers()
      			}
		    });

		    $(".digit-slider").slider({
		    	min: $scope.numberOfValues.minDigit,
      			max: $scope.numberOfValues.maxDigit,
      			value: $scope.numberOfValues.digits,
      			slide: function (evt, ui) {
      				$scope.$apply(function () {
      					$scope.numberOfValues.digits = ui.value;	
      				})
      			},
      			stop: function (evt, ui) {
      				mathfarm.generateNumbers()
      			}
		    });

			// Basic Operators + - × ÷
			$scope.operators = {
				addition: 		{unlock:'', on:true, symbol:'+'},
				subtraction: 	{unlock:'', on:true, symbol:'-'},
				multiplication: {unlock:'', on:true, symbol:'×'},
				division: 		{unlock:'', on:true, symbol:'÷'}
			}

		    // Parentheses (Brackets) settings
		    $scope.parentheses = {
		    	unlock: '',
				on:true, curlvl:0, 
				level: [
					{symbol:{ open:'(', close:')'}},
					{symbol:{ open:'[', close:']'}},
					{symbol:{ open:'{', close:'}'}}
				]
			}

			// Fractions settings
			$scope.fractions = {
				unlock: '',
				on: true, curlvl:0,
				active: false,
				maxlvl:1
			}

			// Algebra
			$scope.algebra = {
				unlock: 'disabled',
				on: false, active: false
			}

			$scope.submitDisabled 	= '';
			$scope.nextUpgrade 		= [];

		    // Users
			$scope.user 			= {};
			$scope.userlvl 			= '';

			$scope.correctAns 		= 0;
			$scope.totalCorrectAns 	= 0;
			$scope.wrongAns 		= 0;
			$scope.questions 		= 0;
			$scope.qnsToNextLevel 	= 0;
			$scope.percentComplete 	= 0;
			
			$scope.logged 			= false;
			$scope.loggedInBadge 	= '';

			// Offline Scenario
			$scope.btnFB 			= 'hidden';
			$scope.btnUser 			= 'hidden';
			$scope.btnOffline 		= '';
			$scope.btnBookmark 		= 'hidden';

		    $scope.hasError 		= "";

		    // Bookmarks
		    $scope.bookmarks 		= {
		    	fbid:'',
		    	longHash:'',
		    	shortHash:'',
		    	list: []
		    }

		    // Alert Panel Stuff
		    $scope.alertPanel = {
		    	messageType: "alert-info",
		    	messageStrong: "Loading",
		    	messageDesp: "Getting your profile, bookmarks and all the good stuff."
		    }

			$scope.IntentLogin = function() {
				if (DM) console.log('%cFunction:','color:#f04','$scope.IntentLogin()');
			    
			    if (!userIsConnected) {
			        $scope.login();
			    }
			};

			$scope.login = function() {
				
		    	if (DM) console.log('%cFunction:','color:#f04','$scope.login()');

			    Facebook.login(function(response) {
			        if (response.status == 'connected') {
			            $scope.logged = true;
			            $scope.me();
			        }

			    });
			};

			$scope.me = function() {

		    	if (DM) console.log('%cFunction:','color:#f04','$scope.me()');

				$scope.btnFB 			= 'hidden';
				$scope.btnUser 			= '';
				$scope.btnOffline 		= 'hidden';
				$scope.btnBookmark 		= '';
				$scope.loggedInBadge 	= 'logged-in';

			    Facebook.api('/me', function(response) {
			        $scope.$apply(function() {
			            $scope.user = response;

			            if ($scope.user != undefined && $scope.user.profilePhoto != undefined) {
			            	$scope.checkUserExist();
			            }
			        });
			    });
			    Facebook.api('/me/picture', function(response) {
			        $scope.$apply(function() {
			            $scope.user.profilePhoto = response.data.url;

			            if ($scope.user != undefined && $scope.user.profilePhoto != undefined) {
			            	$scope.checkUserExist();
			            }
			        });
			    });
			};

			$scope.notLoggedIn = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','$scope.notLoggedIn()');

				mathfarm.generateNumbers();
			}

			$scope.updateUserOperators = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','$scope.updateUserOperators()');

				var _user = {	value: $scope.numberOfValues.length,
								digits: $scope.numberOfValues.digits,
								addition : $scope.operators.addition.on ? 1 : 0,
								additionUnlock : $scope.operators.addition.unlock == 'disabled' ? 0 : 1,
								subtraction : $scope.operators.subtraction.on ? 1 : 0,
								subtractionUnlock : $scope.operators.subtraction.unlock == 'disabled' ? 0 : 1,
								multiplication : $scope.operators.multiplication.on ? 1 : 0,
								multiplicationUnlock : $scope.operators.multiplication.unlock == 'disabled' ? 0 : 1,
								division : $scope.operators.division.on ? 1 : 0,
								divisionUnlock : $scope.operators.division.unlock == 'disabled' ? 0 : 1,
								parenthesis : $scope.parentheses.on ? 1 : 0,
								parenthesisUnlock : $scope.parentheses.unlock == 'disabled' ? 0 : 1,
								fraction : $scope.fractions.on ? 1 : 0,
								fractionUnlock : $scope.fractions.unlock == 'disabled' ? 0 : 1,
								algebra : $scope.algebra.on ? 1 : 0,
								algebraUnlock : $scope.algebra.unlock == 'disabled' ? 0 : 1,
								fbid : $scope.user.id };

				var req = $http({
					method 	: "post",
					url 	: "includes/update.operators.php",
	            	data 	: $.param(_user),
	            	headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
				});
			}

			$scope.updateUserQn = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','$scope.updateUserQn()');

		    	var qnObj = {	correctAns: $scope.correctAns,
		    					totalCorrectAns: $scope.totalCorrectAns,
		    					wrongAns: $scope.wrongAns,
		    					questions: $scope.questions,
		    					lvl: $scope.userlvl,
		    					fbid: $scope.user.id };
				
				var req = $http({
					method	: "post",
	            	url 	: "includes/update.farmer.php",
	            	data 	: $.param(qnObj),
	            	headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
				}).success(
					function () {
						checkLvl();
						calculatePerc();
					}
				)
			}

		    $scope.checkUserExist = function () {   
		    	if (DM) console.log('%cFunction:','color:#f04','$scope.checkUserExist()');
		    	
	            // check if user exist in DB
	            
	            var req = $http({
	            	method	: "post",
	            	url 	: "includes/check.farmer.php",
	            	data 	: $.param($scope.user),
	            	headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	            }).success(
	            	function (ret) {
	            		if (ret == 'found') {
	            			$scope.loadUser();
	            		} else if (ret == 'not found') {
	            			$scope.addUser();
	            		} else if (ret == 'try-again') {
	            			$scope.me();
	            		}
	            	}
	            )
		    }

		    $scope.addUser = function  () {
		    	if (DM) console.log('%cFunction:','color:#f04','$scope.addUser()');
		    	
		    	// check if user exist in DB

		    	var req = $http({
	            	method	: "post",
	            	url 	: "includes/register.farmer.php",
	            	data 	: $.param($scope.user),
	            	headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	            }).success(
	            	function (ret) {
	            		if (DM) console.log('RET:', ret)
	            		if (ret == 'registered') {
	            			$scope.loadUser();
	            		} else if (ret == 'error') {
	            			$scope.me();
	            		}
	            	}
	            )
		    }

		    mathfarm.hideAlert = function () { 
		    	$('.alert-panel').fadeOut(200); 
		    	$scope.showShareFB = 'hidden';
		    }
		    mathfarm.showAlert = function () { 
		    	$('.alert-panel').fadeIn(200); 
		    }

		    $scope.loadBookmarks = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','$scope.loadBookmarks()');

		    	var req = $http({
		    		method 	: "post",
		    		url 	: "includes/load.bookmarks.php",
		    		data 	: $.param($scope.user),
		    		headers	: { 'Content-Type': 'application/x-www-form-urlencoded' } 
		    	}).success(
		    		function (rep) {
		    			$scope.bookmarks.list = [];

		    			$.each(rep, function(i, _rep) {
		    				var bm = {	qn: hashToStr(_rep.longHash), 
		    							sh: _rep.shortHash,
		    							img: _rep.img_url};
		    				$scope.bookmarks.list.push(bm);
		    			});

		    			
	    				mathfarm.hideAlert();
		    		}
		    	)
		    }

		    mathfarm.deleteBookmark = function (qn) {
		    	if (DM) console.log('%cFunction:','color:#f04','mathfarm.deleteBookmark()');

		    	var d = {longHash: strToHash($scope.bookmarks.list[qn].qn), id: $scope.user.id};

		    	$scope.alertPanel.messageType = 'alert-warning';
		    	$scope.alertPanel.messageStrong = 'Deleted';
		    	$scope.alertPanel.messageDesp = 'Question ' + $scope.bookmarks.list[qn].qn + ' have been deleted from your bookmarks';

		    	mathfarm.showAlert();

		    	var req = $http({
		    		method 	: "post",
		    		url 	: "includes/delete.bookmarks.php",
		    		data 	: $.param(d),
		    		headers : {'Content-Type': 'application/x-www-form-urlencoded' }
		    	}).success(
		    		function (ret) {
		    			if (ret == 'deleted') {
		    				$scope.loadBookmarks();
		    			}
		    		}
		    	)
		    }

		    $scope.loadQuestionFromDB = function(sh) {
		    	var dt = {shortHash: sh};

		    	var req = $http({
		    		method	: "post",
		    		url 	: "includes/load.question.php",
		    		data 	: $.param(dt),
		    		headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
		    	}).success(
		    		function (ret) {
		    			if (DM) console.log(ret)
		    			if (DM) console.log(hashToStr(ret));

		    			$scope.mathQn = hashToStr(ret);

		    			$scope.alertPanel = {
					    	messageType: "alert-info",
					    	messageStrong: "Question Loaded",
					    	messageDesp: "Question loaded from a shared link."
					    }
					    
					    mathfarm.showAlert();

		    			prepForBookmark($scope.mathQn);
				    	formatMathJax($scope.mathQn);
				    	calculateAnswer($scope.mathQn);
		    		}
		    	)
		    }

		    $scope.loadUser = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','$scope.loadUser()');

		    	var req = $http({
	            	method	: "post",
	            	url 	: "includes/load.farmer.php",
	            	data 	: $.param($scope.user),
	            	headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	            }).success(
	            	function (ret) {
	            		if (ret == 'try-again') {
	            			$scope.me();
	            		} else {
		    				if (DM) console.log('%cFunction:','color:#fd0','$scope.loadUser(): User Loaded');

	            			$scope.userlvl 					= ret.level;

	            			$scope.correctAns	 			= ret.correctAns;
	            			$scope.totalCorrectAns	 		= ret.totalCorrectAns;
	            			$scope.wrongAns	 				= ret.wrongAns;
	            			$scope.questions 				= ret.question;
	            			
	            			$scope.numberOfValues.digits	= ret.digits;
	            			$scope.numberOfValues.minDigit	= ret.minDigits;
	            			$scope.numberOfValues.maxDigit	= ret.maxDigits;
	            			$scope.numberOfValues.length 	= ret.value;
	            			$scope.numberOfValues.minVal	= ret.minValue;
	            			$scope.numberOfValues.maxVal	= ret.maxValue;

	            			$scope.operators.addition.on				= ret.addition 				== 0 ? false : true;
	            			$scope.operators.addition.unlock			= ret.additionUnlock 		== 0 ? 'disabled' : '';
	            			$scope.operators.subtraction.on				= ret.subtraction 			== 0 ? false : true;
	            			$scope.operators.subtraction.unlock			= ret.subtractionUnlock 	== 0 ? 'disabled' : '';
	            			$scope.operators.multiplication.on			= ret.multiplication 		== 0 ? false : true;
	            			$scope.operators.multiplication.unlock		= ret.multiplicationUnlock 	== 0 ? 'disabled' : '';
	            			$scope.operators.division.on				= ret.division 				== 0 ? false : true;
	            			$scope.operators.division.unlock			= ret.divisionUnlock 		== 0 ? 'disabled' : '';
	            			$scope.parentheses.on						= ret.parenthesis 			== 0 ? false : true;
	            			$scope.parentheses.unlock					= ret.parenthesisUnlock 	== 0 ? 'disabled' : '';
	            			$scope.fractions.on							= ret.fraction 				== 0 ? false : true;
	            			$scope.fractions.unlock						= ret.fractionUnlock 		== 0 ? 'disabled' : '';
	            			$scope.algebra.on							= ret.algebra 				== 0 ? false : true;
	            			$scope.algebra.unlock						= ret.algebraUnlock 		== 0 ? 'disabled' : '';

	            			checkQnsToNextLevel();
	            			
	            			calculatePerc();

	            			mathfarm.generateNumbers();

	            			checkNextLevelUpgrade($scope.userlvl + 1);

	            			$scope.loadBookmarks();
	            		}
	            	}
	            )
		    }

		    $scope.$watch(
		        function() {
		            return Facebook.isReady();
		        },
		        function(newVal) {
		            if (newVal)
		                $scope.facebookReady = true;
		        }
		    );

			$scope.$on('Facebook:statusChange', function(ev, data) {
				
				if (DM) console.log('$scope.$on(Facebook:statusChange)', data, ev);

				$scope.btnFB 	= '';
				$scope.btnUser 	= 'hidden';
				$scope.btnOffline = 'hidden';
				$scope.btnBookmark = 'hidden';

			    if (data.status == 'connected') {
			    	$scope.alertPanel = {
				    	messageType: "alert-info",
				    	messageStrong: "Loading",
				    	messageDesp: "Getting your profile, bookmarks and all the good stuff."
				    }
				    
				    mathfarm.showAlert();

			        $scope.me();
			    } else {
			        $scope.notLoggedIn();
			    }
			});

		    var escapeRegExp 	= function (string) {return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");};
			var replaceAll 		= function (string, find, replace) {return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);}
		    var rand 			= function () {return Math.floor(Math.random()*digits()); }; 
		    var randFromRange 	= function (min,max) {return Math.floor(Math.random()*(max-min+1)+min);};
		    var digits 			= function () {return Math.pow(10, $scope.numberOfValues.digits);};

		    var formatAns		= function (_val){ 
		    	if (DM) console.log('%cFunction:','color:#f04','var formatAns()');

		    	var _mxf = 1 == _val.d ? _val.n : Math.floor(_val.n/_val.d) + " " + _val.n % _val.d + "/" + _val.d,
		    		_imf = 1 == _val.d ? _val.n : _val.n + "/" + _val.d,
		    		_dec = 1 == _val.d ? _val.n : (_val.n/_val.d).toFixed(2);

		    	// console.log(_mxf, _imf, _dec);

		    	if (_val.s == -1) { // its a negative
		    		_mxf = '-' + _mxf;
		    		_imf = '-' + _imf;
		    		_dec = _dec * -1;
		    	}

		    	var ans = {	mixedFraction : _mxf, improperFraction : _imf, decimals : _dec };
				return ans
			};
		    
		    var getOperator = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','var getOperator()');

		    	var ops = [];

		    	$.each($scope.operators, function(operator, settings) {
		    		if (settings.on == true) { ops.push(settings.symbol) }
		    	});

		    	if (ops.length == 0) {
		 			return '+';
		    	} else if (ops.length == 1) {
		 			return ops[0];
		    	} else {
		    		return ops[randFromRange(0,ops.length-1)];
		    	}
		    };

		    var getFraction = function (qn) {
		    	if (DM) console.log('%cFunction:','color:#f04','var getFraction()');

		    	var frac = '';

		    	if ($scope.fractions.on == true ) {
		    		var fracOrNot = randFromRange(0,1);

		    		if (fracOrNot == 1) {
			    		if ($scope.fractions.active == false) {
			    			$scope.fractions.active = true;
			    			frac = '<i class="frac">/</i>';
			    		} else {
			    			$scope.fractions.active = false;
			    			frac = '';
			    		}
		    		} 
		    	} else if ($scope.fractions.on == false) {
		    		$scope.fractions.active = false;
		    	}
				return frac;
		    }

		    var prepForBookmark = function (qn) {
		    	if (DM) console.log('%cFunction:','color:#f04','var prepForBookmark()');

		    	$scope.bookmarks.fbid = $scope.user.id;
		    	$scope.bookmarks.longHash = strToHash(qn);
		    	$scope.bookmarks.shortHash = shortHash();
		    }

		    var hashToStr = function (hash) {
		    	if (DM) console.log('%cFunction:','color:#f04','var hashToStr()');

		    	var hashArray = hash.split('.'),
		    		strFromHash = '';

		    	for (var j = 0; j < hashArray.length; j++) {
		    		var _hash = Number(hashArray[j]) >> 16;
		    		strFromHash += String.fromCharCode(_hash);
		    	};

		    	return strFromHash;
		    }

		    var strToHash = function (qn) {
		    	if (DM) console.log('%cFunction:','color:#f04','var strToHash()');

		    	var str 		= qn.indexOf('<span') == -1 ? qn : $(qn).text(),
		    		hashFromStr = '',
		    		_char 		= '';

		    	for (var i = 0; i < str.length; i++) {
		    		_char = String(str.charCodeAt(i)) << 16;
		    		hashFromStr = i == str.length - 1 ? hashFromStr + _char : hashFromStr + _char + '.';
		    	};

		    	return hashFromStr;
		    }

		    var shortHash = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','var shortHash()');

		    	var len = 7;
		    	var hash = [];
		    	var alphabets = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

		    	var choices = 3

		    	for (var i = 0; i < len; i++) {
		    		
		    		var j = randFromRange(1,choices);
		    		
		    		if (j == 1) { // numbers
		    			hash.push(randFromRange(0,9));
		    		} else if (j == 2) { // lower case
		    			hash.push(alphabets[randFromRange(0,alphabets.length - 1)]);
		    		} else if (j == 3) { // upper case
		    			var a = alphabets[randFromRange(0,alphabets.length - 1)];
		    			a = a.toUpperCase()
		    			hash.push(a);
		    		}	
		    	};

		    	hash = hash.join('');

		    	// console.log(hash);

		    	return hash;
		    }

		    var formatMathJax = function (qn) {
		    	if (DM) console.log('%cFunction:','color:#f04','var formatMathJax()');

		    	var delay 		= 150,
		    		buffer 		= document.getElementById("MathBuffer"),
		    		timeout 	= null,
		    		mjRunning 	= false,
		    		oldText 	= null;

				function Update () {
					if (timeout) {
						clearTimeout(timeout)
					}
					
					timeout = setTimeout(callback, delay);
				}

				function CreatePreview() {

				    timeout = null;
				    if (mjRunning) return;

				    var text = qn.indexOf('<span') == -1 ? qn : $(qn).text();

				    if (text === oldText) return;

				    buffer.innerHTML = oldText = '`' + text + '`';
				    mjRunning = true;
				    
				    MathJax.Hub.Queue(
				        ["Typeset", MathJax.Hub, 'MathBuffer'], 
				        PreviewDone()
				    );
				}

				function PreviewDone () {
					mjRunning = false;
				}

				var callback = MathJax.Callback(CreatePreview());
					callback.autoReset = true; 

		    	$scope.mathJax = qn.indexOf('<span') == -1 ? qn : $(qn).text();

		    	Update();
		    }

		    var getParenthesis = function (i, j) {	
		    	if (DM) console.log('%cFunction:','color:#f04','var getParenthesis()');

		    	var par = {start:"", end:""};

		    	if ($scope.parentheses.on == true) {
			    	var parOrNot = randFromRange(0,1);

			    	if (parOrNot) {
			    		if (i == j && $scope.parentheses.curlvl > 0) {
			    			// if this is the last number and curlvl is not zero
				    		for (var k = 0; k < $scope.parentheses.curlvl; k++) {
			    				par.end = $scope.parentheses.level[k].symbol.close + par.end;
				    		};

				    		par.end = '<i class="par">' + par.end + '</i>';

			    			$scope.parentheses.curlvl = 0;

			    		} else if (i == j && $scope.parentheses.curlvl == 0) {
			    			// if this is the last number and curlvl is zero

			    		} else if ($scope.parentheses.curlvl == 0 || $scope.parentheses.curlvl < ($scope.numberOfValues.length - 1) && $scope.parentheses.curlvl < $scope.parentheses.level.length) {
			    			// if this is not the last number and the curlvl is zero or the par curlvl is less then the number of value and the par curlvl is less then number of par allowed
				    		$scope.parentheses.curlvl++;
				    		par.start = $scope.parentheses.level[$scope.parentheses.curlvl - 1].symbol.open;

				    		par.start = '<i class="par">' + par.start + '</i>';

				    	} else {
				    		// if this is not the last number and the curlvl is not zero
							$scope.parentheses.curlvl--;
							par.end = $scope.parentheses.level[$scope.parentheses.curlvl].symbol.close;

							par.end = '<i class="par">' + par.end + '</i>';

				    	}
			    	} else if ($scope.parentheses.curlvl > 0) {
			    		// if no need par
			    		
			    		for (var k = 0; k < $scope.parentheses.curlvl; k++) {
		    				par.end =  $scope.parentheses.level[k].symbol.close + par.end;
			    		};

			    		par.end = '<i class="par">' + par.end + '</i>';

		    			$scope.parentheses.curlvl = 0;

			    	}

			    }
		    	
		    	return par;
		    }

		    // Start functions not in use -- >
		    var checkQnsToNextLevel = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','var checkQnsToNextLevel()');

		    	// console.log($scope.userlvl, $scope.correctAns, $scope.qnsToNextLevel);

				if ($scope.userlvl == 1) {
				    $scope.qnsToNextLevel = 5;
				} else if ($scope.userlvl == 2) {
				    $scope.qnsToNextLevel = 10;
				} else if ($scope.userlvl == 3) {
				    $scope.qnsToNextLevel = 20;
				} else if ($scope.userlvl == 4) {
				    $scope.qnsToNextLevel = 40;
				} else if ($scope.userlvl == 5) {
				    $scope.qnsToNextLevel = 80;
				} else if ($scope.userlvl == 6) {
				    $scope.qnsToNextLevel = 120;
				} else if ($scope.userlvl == 7) {
				    $scope.qnsToNextLevel = 160;
				} else if ($scope.userlvl == 8) {
				    $scope.qnsToNextLevel = 200;
				} else if ($scope.userlvl == 9) {
				    $scope.qnsToNextLevel = 240;
				} else if ($scope.userlvl == 10) {
				    $scope.qnsToNextLevel = 280;
				} else if ($scope.userlvl == 11) {
				    $scope.qnsToNextLevel = 320;
				} else if ($scope.userlvl == 12) {
				    $scope.qnsToNextLevel = 360;
				} else if ($scope.userlvl == 13) {
				    $scope.qnsToNextLevel = 400;
				} else if ($scope.userlvl == 14) {
				    $scope.qnsToNextLevel = 440;
				} else if ($scope.userlvl == 15) {
				    $scope.qnsToNextLevel = 480;
				} else if ($scope.userlvl == 16) {
				    $scope.qnsToNextLevel = 520;
				} else if ($scope.userlvl == 17) {
				    $scope.qnsToNextLevel = 560;
				} else if ($scope.userlvl == 18) {
				    $scope.qnsToNextLevel = 600;
				} else if ($scope.userlvl == 19) {
				    $scope.qnsToNextLevel = 640;
				} else if ($scope.userlvl == 20) {
				    $scope.qnsToNextLevel = 680;
				}

				checkLvlUpgrade($scope.userlvl);
		    }

		    var checkNextLevelUpgrade = function (lvl) {
		    	if (DM) console.log('%cFunction:','color:#f04','var checkNextLevelUpgrade()');
		    	
		    	var unlocks = [];

		    	if (lvl == 2) {
				    unlocks.push('Final Year Math Paper for Primary 5, Siling Primary School');
				    
				} else if (lvl == 3) {
				    unlocks.push('Final Year Math Paper for Primary 5, Siling Primary School');
				    
				} else if (lvl == 4) {
				    unlocks.push('Final Year Math Paper for Primary 5, Siling Primary School');
				    
				} else if (lvl == 5) {
				    unlocks.push('Final Year Math Paper for Primary 5, Siling Primary School');
				    
				} else if (lvl == 6) {
				    unlocks.push('Final Year Math Paper for Primary 5, Siling Primary School');
				    
				} else if (lvl == 7) {
				    unlocks.push('Final Year Math Paper for Primary 5, Siling Primary School');
				    
				} else if (lvl == 8) {
				    unlocks.push('Final Year Math Paper for Primary 5, Siling Primary School');
				    
				} else if (lvl == 9) {
				    unlocks.push('Final Year Math Paper for Primary 5, Siling Primary School');
				    
				} else if (lvl == 10) {
				    unlocks.push('Final Year Math Paper for Primary 5, Siling Primary School');
				    
				} else if (lvl == 11) {
				    unlocks.push('Final Year Math Paper for Primary 5, Siling Primary School');
				    
				} else if (lvl == 12) {
				    
				} else if (lvl == 13) {
				    
				} else if (lvl == 14) {
				    
				} else if (lvl == 15) {
				    
				} else if (lvl == 16) {
				    
				} else if (lvl == 17) {
				    
				} else if (lvl == 18) {
				    
				} else if (lvl == 19) {
				    
				} else if (lvl == 20) {
				    
				}

		    	$scope.nextUpgrade = unlocks;
		    }

		    var checkLvlUpgrade = function (lvl) {
		    	if (DM) console.log('%cFunction:','color:#f04','var checkLvlUpgrade()');

		    	if (lvl == 2) {
		    		// subtraction
				    $scope.operators.subtraction.unlock = '';
				    $scope.operators.subtraction.on = true;

				    // increase value to 3 values
				    $scope.numberOfValues.length = 3;
				    $scope.numberOfValues.maxVal = 3;
				    
				} else if (lvl == 3) {
					// increase digits to 2 digits
				    $scope.numberOfValues.digits = 2;
				    $scope.numberOfValues.maxDigit = 2;
				    
				} else if (lvl == 4) {
					// unlock multiplication
				    $scope.operators.multiplication.unlock = '';
				    $scope.operators.multiplication.on = true;
				    
				} else if (lvl == 5) {
					// increase value to 4 values
				    $scope.numberOfValues.length = 4;
				    $scope.numberOfValues.maxVal = 4;
				    
				} else if (lvl == 6) {
					// unlock division
				    $scope.operators.division.unlock = '';
				    $scope.operators.division.on = true;
				    
				} else if (lvl == 7) {
					// increase digits to 3 digits
				    $scope.numberOfValues.digits = 3;
				    $scope.numberOfValues.maxDigit = 3;
				    
				} else if (lvl == 8) {
					// unlock parenthesis
				    $scope.operators.parentheses.unlock = '';
				    $scope.operators.parentheses.on = true;
				    
				} else if (lvl == 9) {
					// increase value to 5 values
				    $scope.numberOfValues.length = 5;
				    $scope.numberOfValues.maxVal = 5;
				    
				} else if (lvl == 10) {
					// unlock fractions
				    $scope.operators.fraction.unlock = '';
				    $scope.operators.fraction.on = true;
				    
				} else if (lvl == 11) {
					// increase digits to 4 digits
				    $scope.numberOfValues.digits = 4;
				    $scope.numberOfValues.maxDigit = 4;
				    
				} else if (lvl == 12) {
				    
				} else if (lvl == 13) {
				    
				} else if (lvl == 14) {
				    
				} else if (lvl == 15) {
				    
				} else if (lvl == 16) {
				    
				} else if (lvl == 17) {
				    
				} else if (lvl == 18) {
				    
				} else if (lvl == 19) {
				    
				} else if (lvl == 20) {
				    
				}
		    }

		    var checkLvl = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','var checkLvl()');

		    	if ($scope.correctAns == $scope.qnsToNextLevel && $scope.qnsToNextLevel != 0) {
		    		$scope.userlvl++;
		    		// TO DO issue with reset correctAns when go up level
		    		$scope.correctAns = 0;
		    		checkQnsToNextLevel();
		    		calculatePerc();
		    		$scope.updateUserOperators();
		    		$scope.updateUserQn();

		    		$('.nextIndex').hide();

		    		$('#unlock').modal('show');
		    	}
		    }

		    var calculatePerc = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','var calculatePerc()');

		    	$scope.percentComplete = $scope.correctAns / $scope.qnsToNextLevel * 100;
		    }
		    // End unused functions --> 

		    var calculateAnswer = function (qn) {
		    	if (DM) console.log('%cFunction:','color:#f04','var calculateAnswer()');

				var parser = math.parser();
				qn = qn.indexOf('<span') == -1 ? qn : $(qn).text();
				qn = replaceAll(qn, '×', '*');
				qn = replaceAll(qn, '÷', '/');
				qn = replaceAll(qn, '{', '(');
				qn = replaceAll(qn, '[', '(');
				qn = replaceAll(qn, '}', ')');
				qn = replaceAll(qn, ']', ')');

				var ans = parser.eval(qn);
					ans = formatAns(math.fraction(ans));

				if (DM) console.log('%cQUESTION:','color: red',qn);
				if (DM) console.log('%cANSWER:','color: blue',ans);

				mathfarm.correctAnswer = ans;
		    }

		    var generateQuestion = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','mathfarm.generateQuestion()');

		    	var qn = "",
		    		theresFrac = false;

		    	for (var i = 0; i < mathfarm.numbers.length; i++) {

		    		var frac 		= i == 0 ? '' : getFraction();
		    		var ops 		= i == 0 ? '' : '<i class="ops">' + getOperator() + '</i>';
			    		ops 		= frac != '' ? '' : ops;
		    			theresFrac 	= frac != '' ? true : theresFrac;
		    		var par 		= getParenthesis(i, mathfarm.numbers.length - 1),
		    			parStartAndOps;

		    		if (String(ops) != '' && String(par.start) != '') {
		    			parStartAndOps = String(ops) + String(par.start);
		    		} else {
		    			parStartAndOps = String(par.start) + String(ops);
		    		}

		    		qn += '<span class="num-grp">' + String(frac) + String(parStartAndOps) + '<i class="num">' + String(mathfarm.numbers[i]) + '</i>' + String(par.end) + "</span>";
		    		// qn += String(frac) + String(parStartAndOps) + String(mathfarm.numbers[i]) + String(par.end);
		    	};

		    	$scope.mathQn = qn;

		    	if (DM) console.log(qn);

		    	prepForBookmark(qn);
		    	formatMathJax(qn);
		    	calculateAnswer(qn);

		    	$scope.updateUserOperators();
		    }

		    mathfarm.loadQuestion = function (qn) {
		    	if (DM) console.log('%cFunction:','color:#f04','mathfarm.loadQuestion()');
		    	
		    	$scope.mathQn = $scope.bookmarks.list[qn].qn;

		    	if (DM) console.log($scope.mathQn);

		    	prepForBookmark($scope.mathQn);
		    	formatMathJax($scope.mathQn);
		    	calculateAnswer($scope.mathQn);
		    }

		    mathfarm.shareQuestion = function (qn) {
		    	if (DM) console.log('%cFunction:','color:#f04','mathfarm.shareQuestion()');
		    	// if (DM) console.log($scope.bookmarks.list[qn].sh);

		    	$scope.showShareFB = '';

		    	$scope.alertPanel.messageType = 'alert-info';
		    	$scope.alertPanel.messageStrong = 'Share Link: ';
		    	$scope.alertPanel.messageDesp = 'http://www.math-farm.com/#/' + $scope.bookmarks.list[qn].sh;

				mathfarm.showAlert();
		    	$scope.showShareFBQn = qn;
		   		
		    	// ping GA to track custom event
		    }

		    mathfarm.shareFB = function (qn) {
		    	if (DM) console.log('%cFunction:','color:#f04','mathfarm.shareFB(' + qn +')');
		    	
		    	var base_u = document.domain == 'mathfarm.col-md-12.com' ? 'http://mathfarm.col-md-12.com/' : 'http://www.math-farm.com/';

		    	var img_u = $scope.bookmarks.list[qn].img.replace('../', '');

		    	FB.ui({
					method: 'share',
					href: base_u + '#/' + $scope.bookmarks.list[qn].sh,
					caption: 'Do you think you can solve this math question?',
					picture: base_u + img_u,
				}, function(response){});
		    }

		    mathfarm.addToBookmark = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','mathfarm.addToBookmark()');

    			$scope.alertPanel.messageType = 'alert-info';
		    	$scope.alertPanel.messageStrong = 'Adding bookmark:';
		    	$scope.alertPanel.messageDesp = 'saving this question to the bookmark list';

		    	var data_to_send = {fbid: $scope.bookmarks.fbid, 
		    						longHash: $scope.bookmarks.longHash,
		    						shortHash: $scope.bookmarks.shortHash,
		    						imgVal:""}

		    	$('.input-grp').hide()

				mathfarm.hideAlert();

		    	html2canvas(document.getElementsByClassName('chalk-bg'), {
		    		background:'#000',
		    		onrendered: function (canvas) {	

		    			data_to_send.imgVal = canvas.toDataURL("image/png");

		    			$('.input-grp').show()
		    			mathfarm.showAlert();

			            var req = $http({
							method 	: "post",
							url 	: "includes/add.bookmarks.php",
			            	data 	: $.param(data_to_send),
			            	headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
						}).success(
							function (rep) {
				    			$scope.alertPanel.messageType = 'alert-info';
						    	$scope.alertPanel.messageStrong = 'Bookmarks Added';
						    	$scope.alertPanel.messageDesp = 'Question ' + hashToStr($scope.bookmarks.longHash) + ' have been added to your bookmarks';

						    	mathfarm.showAlert();

				    			$scope.loadBookmarks();
							}
						)
			        }
			    });
		    }

		    mathfarm.generateNumbers = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','mathfarm.generateNumbers()');

		    	if(window.location.hash) {
					var h = window.location.hash.replace('#/','');
					$scope.loadQuestionFromDB(h);
				} else {
					
					// Hash fragment doesn't exist

			    	if (isNaN($scope.numberOfValues.length) || $scope.numberOfValues.length < $scope.numberOfValues.minVal || $scope.numberOfValues.length > $scope.numberOfValues.maxVal) {
			    		$scope.numberOfValues.length = $scope.numberOfValues.minVal;
			    	}

			    	if (isNaN($scope.numberOfValues.digits) || $scope.numberOfValues.digits < $scope.numberOfValues.minDigit || $scope.numberOfValues.digits > $scope.numberOfValues.maxVal) {
			    		$scope.numberOfValues.digits = $scope.numberOfValues.minDigit;
			    	}

			    	mathfarm.numbers = [];
			    	for (var i = 0; i < $scope.numberOfValues.length; i++) {
			    		mathfarm.numbers.push(randFromRange(1,digits()));
			    	};
			    	
			    	generateQuestion();
				}
		    }

		    mathfarm.nextQuestion = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','mathfarm.nextQuestion()');

		    	$('.qn-proc').remove();

		    	$scope.hasError = '';
				$scope.nextQuestionMsg = "Skip";
				mathfarm.answerProvided = '';

				if(window.location.hash) {
					window.location.hash = '';
				}

				mathfarm.hideAlert();

				checkLvl();

		    	mathfarm.generateNumbers();

		    	checkNextLevelUpgrade($scope.userlvl + 1);

	    		$('.nextIndex').show();
		    	$('.btn.submit').show();
		    }

		    mathfarm.checkAnswer = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','mathfarm.checkAnswer()');


				if (mathfarm.answerProvided == '' ) {
					$scope.alertPanel = {
				    	messageType: "alert-warning",
				    	messageStrong: "Please provide an answer",
				    	messageDesp: ""
				    }

				    mathfarm.showAlert();

				} else if (mathfarm.answerProvided == mathfarm.correctAnswer.mixedFraction || mathfarm.answerProvided == mathfarm.correctAnswer.improperFraction || mathfarm.answerProvided == mathfarm.correctAnswer.decimals) {

		    		if (DM) console.log('%cCORRECT ANSWER','color:green;');
		    		$scope.hasError = 'has-success';
					$scope.nextQuestionMsg = "Next Question";
					
					$scope.correctAns++;
					$scope.totalCorrectAns++;
        			$scope.questions++;

					$('.btn.submit').hide();

		    	} else {
		    		if (DM) console.log('%cWRONG ANSWER','color:red;');
					$scope.nextQuestionMsg = "Try Next?";
		    		$scope.hasError = 'has-error';

        			$scope.wrongAns++;
        			$scope.questions++;
				
					$('.btn.submit').hide();
		    	}

		    	checkLvl();
		    	$scope.updateUserQn();
		    }

		    var checkURLforHash = function () {
		    	if (DM) console.log('%cFunction:','color:#f04','var.checkURLforHash()');
		    }

		    // mathfarm.generateNumbers();

		    Facebook.getLoginStatus(function(response) {
				
		    	if (DM) console.log('%cFunction:','color:#f04','Facebook.getLoginStatus()', response);
		        
		        if (response.status == 'connected') {
		            userIsConnected = true;
		        } else if (response.status == 'unknown' || response.status == 'not_authorized') {
		            userIsConnected = false;

		            $scope.btnFB 	= '';
					$scope.btnUser 	= 'hidden';
					$scope.btnOffline = 'hidden';
					$scope.btnBookmark = 'hidden';

					$scope.alertPanel = {
				    	messageType: "alert-warning",
				    	messageStrong: "Not Logged In",
				    	messageDesp: "Won't be able to bookmark and share the questions."
				    }

				    mathfarm.generateNumbers();

		        }
		    });

		}]);
	})(window.angular);