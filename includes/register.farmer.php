<?php

include_once 'db_connect.php';
include_once 'psl-config.php';

if (isset($_POST['name'], $_POST['id'], $_POST['profilePhoto'])) {
    // Sanitize and validate the data passed in
    $username               = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $fbid                   = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $profilePhoto           = filter_input(INPUT_POST, 'profilePhoto', FILTER_SANITIZE_STRING);
    $level                  = 1;
    $questions              = 0;
    $correctAns             = 0;
    $totalCorrectAns        = 0;
    $wrongAns               = 0;
    
    $value                  = 2;
    $minValue               = 2;
    $maxValue               = 10;

    $digits                 = 1;
    $minDigits              = 1;
    $maxDigits              = 3;
    
    $addition               = 1;
    $additionUnlock         = 1;

    $subtraction            = 0;
    $subtractionUnlock      = 1;

    $multiplication         = 0;
    $multiplicationUnlock   = 1;

    $division               = 0;
    $divisionUnlock         = 1;

    $parenthesis            = 0;
    $parenthesisUnlock      = 1;
    
    $fraction               = 0;
    $fractionUnlock         = 1;
    
    $algebra               = 0;
    $algebraUnlock         = 1;

    $insert_stmt = $mysqli->prepare("INSERT INTO `farmers` (username, fbid, profilePhoto, level, questions, correctAns, totalCorrectAns, wrongAns, value, minVal, maxVal, digits, minDigits, maxDigits, addition, additionUnlock, subtraction, subtractionUnlock, multiplication, multiplicationUnlock, division, divisionUnlock, parenthesis, parenthesisUnlock, fraction, fractionUnlock, algebra, algebraUnlock) 
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    if ( false===$insert_stmt ) {
        die('prepare() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $insert_stmt->bind_param('sssiiiiiiiiiiiiiiiiiiiiiiiii', $username, $fbid, $profilePhoto, $level, $questions, $correctAns, $totalCorrectAns, $wrongAns, $value, $minValue, $maxValue, $digits, $minDigits, $maxDigits, $addition, $additionUnlock, $subtraction, $subtractionUnlock, $multiplication, $multiplicationUnlock, $division, $divisionUnlock, $parenthesis, $parenthesisUnlock, $fraction, $fractionUnlock, $algebra, $algebraUnlock);

    if ( false===$rc ) {
        die('bind_param() failed: ' . htmlspecialchars($insert_stmt->error));
    }

    $rc = $insert_stmt->execute();
    if ( false===$rc ) {
        die('execute() failed: ' . htmlspecialchars($insert_stmt->error));
    }
    echo "registered";
    
    exit();
} else {
    echo "error";
}