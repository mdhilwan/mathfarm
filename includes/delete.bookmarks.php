<?php

include_once 'db_connect.php';
include_once 'psl-config.php';

if (isset($_POST['id'], $_POST['longHash'])) {
    // Sanitize and validate the data passed in
    $fbid               = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $longHash           = filter_input(INPUT_POST, 'longHash', FILTER_SANITIZE_STRING);

    $delete_stmt = $mysqli->prepare("DELETE FROM `bookmarks`
                                     WHERE fbid = ? AND longHash = ?");

    if ( false===$delete_stmt ) {
        die('prepare() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $delete_stmt->bind_param('ss', $fbid, $longHash);

    if ( false===$rc ) {
        die('bind_param() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $delete_stmt->execute();
    
    if ( false===$rc ) {
        die('execute() failed: ' . htmlspecialchars($mysqli->error));
    }

    echo "deleted";

    exit();
} else {
    echo "try-again";
}