<?php

include_once 'db_connect.php';
include_once 'psl-config.php';

if (isset($_POST['shortHash'])) {
    // Sanitize and validate the data passed in
    $shortHash               = filter_input(INPUT_POST, 'shortHash', FILTER_SANITIZE_STRING);

    $select_stmt = $mysqli->prepare("SELECT `longHash`
                                     FROM `bookmarks`
                                     WHERE shortHash = ?");

    if ( false===$select_stmt ) {
        die('prepare() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $select_stmt->bind_param('s', $shortHash);

    if ( false===$rc ) {
        die('bind_param() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $select_stmt->bind_result($longHash);


    if ( false===$rc ) {
        die('bind_result() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $select_stmt->execute();
    
    if ( false===$rc ) {
        die('execute() failed: ' . htmlspecialchars($mysqli->error));
    }

    $select_stmt->store_result();
    $select_stmt->fetch();

    echo $longHash;

    exit();
} else {
    echo "try-again";
}