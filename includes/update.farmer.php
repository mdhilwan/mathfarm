<?php

include_once 'db_connect.php';
include_once 'psl-config.php';

if (isset($_POST['correctAns'], $_POST['wrongAns'], $_POST['questions'], $_POST['fbid'], $_POST['lvl'], $_POST['totalCorrectAns'])) {
    // Sanitize and validate the data passed in
    $correctAns         = $_POST['correctAns'];
    $totalCorrectAns         = $_POST['totalCorrectAns'];
    $wrongAns           = $_POST['wrongAns'];
    $questions          = $_POST['questions'];
    $lvl                = $_POST['lvl'];
    $fbid               = filter_input(INPUT_POST, 'fbid', FILTER_SANITIZE_STRING);

    if ($select_stmt = $mysqli->prepare("UPDATE  `farmers` SET  `questions` =  ?, `level` =  ?, `correctAns` = ?, `totalCorrectAns` = ?, `wrongAns` = ? WHERE `fbid` = ?")) {
        $select_stmt->bind_param('iiiiis', $questions, $lvl, $correctAns, $totalCorrectAns, $wrongAns, $fbid);
        $status = $select_stmt->execute();
    }

    exit();
} else {
    echo "try-again";
}