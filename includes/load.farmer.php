<?php

include_once 'db_connect.php';
include_once 'psl-config.php';

$user = [];

if (isset($_POST['name'], $_POST['id'], $_POST['profilePhoto'])) {
    // Sanitize and validate the data passed in
    $username       = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $fbid           = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $profilePhoto   = filter_input(INPUT_POST, 'profilePhoto', FILTER_SANITIZE_STRING);

    $select_stmt = $mysqli->prepare("SELECT `id`, `fbid`, `username`, `profilePhoto`, `level`, `questions`, `correctAns`, `totalCorrectAns`, `wrongAns`, `value`, `minVal`, `maxVal`, `digits`, `minDigits`, `maxDigits`, `addition`, `additionUnlock`, `subtraction`, `subtractionUnlock`, `multiplication`, `multiplicationUnlock`, `division`, `divisionUnlock`, `parenthesis`, `parenthesisUnlock`, `fraction`, `fractionUnlock`, `algebra`, `algebraUnlock`
                                         FROM `farmers` 
                                         WHERE fbid = ?");

    if ( false===$select_stmt ) {
        die('prepare() failed: ' . htmlspecialchars($select_stmt->error));
    }

    $rc = $select_stmt->bind_param('s', $fbid);

    if ( false===$rc ) {
        die('bind_param() failed: ' . htmlspecialchars($select_stmt->error));
    }

    $rc = $select_stmt->bind_result($id,$fbid,$username,$profilePhoto,$level,$question,$correctAns,$totalCorrectAns,$wrongAns,$value,$minValue,$maxValue,$digits,$minDigits,$maxDigits,$addition,$additionUnlock,$subtraction,$subtractionUnlock,$multiplication,$multiplicationUnlock,$division,$divisionUnlock,$parenthesis,$parenthesisUnlock,$fraction,$fractionUnlock,$algebra,$algebraUnlock);
        
    if ( false===$rc ) {
        die('bind_result() failed: ' . htmlspecialchars($select_stmt->error));
    }

    $rc = $select_stmt->execute();

    if ( false===$rc ) {
        die('execute() failed: ' . htmlspecialchars($select_stmt->error));   
    }

    $select_stmt->store_result();
    $select_stmt->fetch();

    $user = [
        id => $id,
        fbid => $fbid,
        username => $username,
        profilePhoto => $profilePhoto,
        level => $level,
        question => $question,
        correctAns => $correctAns,
        totalCorrectAns => $totalCorrectAns,
        wrongAns => $wrongAns,
        value => $value,
        minValue => $minValue,
        maxValue => $maxValue,
        digits => $digits,
        minDigits => $minDigits,
        maxDigits => $maxDigits,
        addition => $addition,
        additionUnloc => $additionUnlock,
        subtraction => $subtraction,
        subtractionUnlock => $subtractionUnlock,
        multiplication => $multiplication,
        multiplicationUnlock => $multiplicationUnlock,
        division => $division,
        divisionUnlock => $divisionUnlock,
        parenthesis => $parenthesis,
        parenthesisUnlock => $parenthesisUnlock,
        fraction => $fraction,
        fractionUnlock => $fractionUnlock,
        algebra => $algebra,
        algebraUnlock => $algebraUnlock
    ];

    echo json_encode($user);
    exit();
} else {
    echo "try-again";
}