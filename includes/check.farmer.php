<?php

include_once 'db_connect.php';
include_once 'psl-config.php';

if (isset($_POST['name'], $_POST['id'], $_POST['profilePhoto'])) {
    // Sanitize and validate the data passed in
    $username       = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $fbid           = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $profilePhoto   = filter_input(INPUT_POST, 'profilePhoto', FILTER_SANITIZE_STRING);

    if ($select_stmt = $mysqli->prepare("SELECT * FROM `farmers` WHERE fbid = ? AND username = ?")) {
        $select_stmt->bind_param('ss', $fbid, $username);
        $select_stmt->execute();
        $select_stmt->store_result();
        $numrows = $select_stmt->num_rows;

        if ($numrows == '0') {
            echo "not found";
        } else {
            echo "found";
        }
    }

    exit();
} else {
    echo "try-again";
}