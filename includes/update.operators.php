<?php

include_once 'db_connect.php';
include_once 'psl-config.php';

if (isset($_POST['addition'],$_POST['additionUnlock'],$_POST['subtraction'],$_POST['subtractionUnlock'],$_POST['multiplication'],$_POST['multiplicationUnlock'],$_POST['division'],$_POST['divisionUnlock'],$_POST['parenthesis'],$_POST['parenthesisUnlock'],$_POST['fraction'],$_POST['fractionUnlock'],$_POST['fbid'])) {
    // Sanitize and validate the data passed in
    
    $addition               = $_POST['addition'];
    $additionUnlock         = $_POST['additionUnlock'];
    $value                  = $_POST['value'];
    $digits                 = $_POST['digits'];
    $subtraction            = $_POST['subtraction'];
    $subtractionUnlock      = $_POST['subtractionUnlock'];
    $multiplication         = $_POST['multiplication'];
    $multiplicationUnlock   = $_POST['multiplicationUnlock'];
    $division               = $_POST['division'];
    $divisionUnlock         = $_POST['divisionUnlock'];
    $parenthesis            = $_POST['parenthesis'];
    $parenthesisUnlock      = $_POST['parenthesisUnlock'];
    $fraction               = $_POST['fraction'];
    $fractionUnlock         = $_POST['fractionUnlock'];
    $algebra               = $_POST['algebra'];
    $algebraUnlock         = $_POST['algebraUnlock'];
    $fbid                   = filter_input(INPUT_POST, 'fbid', FILTER_SANITIZE_STRING);

    $select_stmt = $mysqli->prepare("UPDATE  `farmers` 
                                         SET  `value` = ?, `digits` = ?, `addition` = ?, `additionUnlock` = ?, `subtraction` = ?, `subtractionUnlock` = ?, `multiplication` = ?, `multiplicationUnlock` = ?, `division` = ?, `divisionUnlock` = ?, `parenthesis` = ?, `parenthesisUnlock` = ?, `fraction` = ?, `fractionUnlock` = ?, `algebra` = ?, `algebraUnlock` = ?
                                         WHERE `fbid` = ?");

    if ( false===$select_stmt ) {
        die('prepare() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $select_stmt->bind_param('iiiiiiiiiiiiiiiis', $value, $digits, $addition, $additionUnlock, $subtraction, $subtractionUnlock, $multiplication, $multiplicationUnlock, $division, $divisionUnlock, $parenthesis, $parenthesisUnlock, $fraction, $fractionUnlock, $algebra, $algebraUnlock, $fbid);

    if ( false===$rc ) {
        die('bind_param() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $select_stmt->execute();

    if ( false===$rc ) {
        die('execute() failed: ' . htmlspecialchars($mysqli->error));
    }
    
    echo "updated";

    exit();
} else {
    echo "try-again";
}