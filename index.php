<!DOCTYPE html>
<html lang="en" ng-app="mathfarmApp">

<head>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/mathjs/2.3.0/math.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/js.js"></script>
	<script src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=AM_HTMLorMML-full" type="text/javascript"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.6/angular.min.js"></script>
	<script src="http://cdn.firebase.com/js/client/2.2.4/firebase.js"></script>
	<script src="http://cdn.firebase.com/libs/angularfire/1.1.3/angularfire.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.6/angular-cookies.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.6/angular-sanitize.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
	<script src="js/angular-facebook.min.js"></script>
	<script src="js/mathfarm.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="Generate unlimited amount of math question as many times as you want" />
	<meta name="keywords" content="math, generator, unlimited questions, generate question, random questions, random math" />
	<meta name="author" content="Col-md-12.com">
	<meta name="robots" content="index, follow">
	<meta name="revisit-after" content="3 month">

    <title>Math Farm</title>
    <meta property="og:image" content="http://www.math-farm.com/img/mathfarm-1024.jpg">
	<meta property="og:image:type" content="image/jpg">
	<meta property="og:image:width" content="1024">
	<meta property="og:image:height" content="1024">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/default.min.css" rel="stylesheet">
    <link href="css/index.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" >
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-68934854-1', 'auto');
		ga('send', 'pageview');
	</script>
</head>

<body>
	<div class="ng-wrapper" ng-controller="mathfarmController as mathfarm">
		<?php include 'nav-bar.html' ?>
	    <div class="chalk-bg animate-this {{hasError}}">
	    	<div class="alert-container">
		    	<div class="container">
		    		<div class="col-md-18 col-md-offset-3 alert-wrapper">
		    			<div class="alert-panel alert {{alertPanel.messageType}} alert-dismissible marginTop20" role="alert">
		    				<button type="button" class="close" ng-click="mathfarm.hideAlert()">
		    					<span aria-hidden="true">&times;</span>
		    				</button>
		    				<strong>{{alertPanel.messageStrong}}</strong> <span ng-bind-html="alertPanel.messageDesp"></span> <span class=""><a class="{{showShareFB}} badge fb-share" ng-click="mathfarm.shareFB(showShareFBQn)">Share on <i class="fa fa-facebook-official"></i></a></span>
		    			</div>
		    		</div>
		    	</div>
	    	</div>
		    <div class="container-fluid paddingTop50">
		    	<!-- Farming Normal -->
		    	<div class="main-content farming-normal">
		    		<div class="form-inline text-center">
		    			<div class="form-group form-group-lg">
		    				<label for="myanswer" class="control-label question-wrapper">
		    					<div class="question control-label hidden" ng-bind-html="mathQn"></div>
								
		    					<div id="MathBuffer" class="question question-mathjax dont-animate-this"></div>
		    					
		    					<span class="input-grp">
	    							<input class="form-control" id="myanswer" type="number" placeholder="Answer" ng-model="mathfarm.answerProvided">
		    						<a href="" class="btn btn-lg submit btn-info" ng-click="mathfarm.checkAnswer()">Enter</a>
									<a href="" class="btn btn-lg randomise btn-success" ng-click="mathfarm.nextQuestion()">{{nextQuestionMsg}}</a>
									<a href="" class="btn btn-lg bookmark btn-warning {{btnBookmark}}" ng-click="mathfarm.addToBookmark()"><span class="glyphicon glyphicon-bookmark"></span></a>
								</span>
		    				</label>
		    			</div>
		    		</div>
		    	</div>
		    </div>
	   	</div>
	   	<div class="container">
    		<p>Answer can be in improper fractions or mixed fractions or closest to 2 decimals points.</p>
    		<input type="text" ng-model="data.foo" />
    		<h1>You said: {{data.foo}}</h1>	
    		<div class="row hidden">
	    		<div class="col-sm-24 col-md-18">
	    			<h6>Current Level: 
	    				<div class="badge badge-level {{loggedInBadge}}">{{userlvl}}</div> 
	    			</h6>
					<p>Next level: {{userlvl + 1}}. Unlock: 
						<ul class="nextIndex">
							<li ng-repeat="n in nextUpgrade">
								<kbd>{{n}}</kbd>
							</li>
						</ul>
					</p>
					<h6>Math Questions Completed: <kbd>{{correctAns}}/{{qnsToNextLevel}}</kbd></h6>
	    			<div class="progress">
						<div class="progress-bar progress-bar-striped progress-bar-info" style="width: {{percentComplete}}%;"></div>
					</div>
	    		</div>
    		</div>
	    </div>

	    <!-- Todo:
		- info box to introduce user to mathfarm
		- talk abt the different levels
	     -->
     	<?php include 'modal.html' ?>
    </div>
	<footer class="footer animate-this">
		<div class="container">
			<p class="text-muted">Math Farm is brought to you by <a href="http://www.col-md-12.com" target="_blank"><img class="footer-logo" src="img/col-md-12-b.png" alt=""></a>.</p>
		</div>
	</footer>

</body>

</html>