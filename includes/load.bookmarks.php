<?php

include_once 'db_connect.php';
include_once 'psl-config.php';

$bookmarks = array();

if (isset($_POST['id'])) {
    // Sanitize and validate the data passed in
    $fbid               = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);

    $select_stmt = $mysqli->prepare("SELECT `longHash`, `shortHash`,`img_url`
                                     FROM `bookmarks`
                                     WHERE fbid = ?");

    if ( false===$select_stmt ) {
        die('prepare() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $select_stmt->bind_param('s', $fbid);

    if ( false===$rc ) {
        die('bind_param() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $select_stmt->bind_result($longHash, $shortHash, $img_url);


    if ( false===$rc ) {
        die('bind_result() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $select_stmt->execute();
    
    if ( false===$rc ) {
        die('execute() failed: ' . htmlspecialchars($mysqli->error));
    }

    while ($select_stmt->fetch()) {
        $bookmarks[] = [
            longHash => $longHash, 
            shortHash => $shortHash,
            img_url => $img_url
        ];
    }

    echo json_encode($bookmarks);

    exit();
} else {
    echo "try-again";
}