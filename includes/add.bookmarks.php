<?php

include_once 'db_connect.php';
include_once 'psl-config.php';

define('UPLOAD_DIR', '../img/bookmarks/');

if (isset($_POST['fbid'], $_POST['longHash'], $_POST['shortHash'], $_POST['imgVal'])) {
    // Sanitize and validate the data passed in
    $fbid               = filter_input(INPUT_POST, 'fbid', FILTER_SANITIZE_STRING);
    $longHash           = filter_input(INPUT_POST, 'longHash', FILTER_SANITIZE_STRING);
    $shortHash          = filter_input(INPUT_POST, 'shortHash', FILTER_SANITIZE_STRING);
    $img                = $_POST['imgVal'];

    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = UPLOAD_DIR . uniqid() . '.png';
    $success = file_put_contents($file, $data);

    if (!$success) {
        die('Unable to save the file.');
    }

    $insert_stmt = $mysqli->prepare("INSERT INTO `bookmarks` (fbid, longHash, shortHash, img_url) VALUES (?, ?, ?, ?)");

    if ( false===$insert_stmt ) {
        die('prepare() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $insert_stmt->bind_param('ssss', $fbid, $longHash, $shortHash, $file);

    if ( false===$rc ) {
        die('bind_param() failed: ' . htmlspecialchars($mysqli->error));
    }

    $rc = $insert_stmt->execute();
    
    if ( false===$rc ) {
        die('execute() failed: ' . htmlspecialchars($mysqli->error));
    }

    echo "added".$file;


    exit();
} else {
    echo "try-again";
}