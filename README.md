# MATHFARM #

The concept is to create a tool for anybody to use to practice mathematics. The tool will randomly generate math problems to solve. From simple additions to more complicated equations and algebras.

## Framework ##
* Angular JS for the databinding the processing of data and information
* Twitter Bootstrap for the Grids
* PHP MySQL for the database
* MathJAX for rendering of the math problems
* MathJS for math processing

### Todo: ###
* Saving to MySQL
* Saving of qns answered correctly or wrongly
* Saving of settings
* Keeping track of the level
* Bookmark panel

### Done: ###
* Login with Facebook
* Addition, Subtraction, Multiplication, Division, Parenthesis, Fraction
* Random number generator